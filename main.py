import scrcpy
#import cv2
import easyocr
import time

from PIL import Image
from PIL import ImageFilter
from PIL import ImageEnhance
from adbutils import adb
from playsound import playsound

#           x    y     x     y
gold_pos = (190, 140, 370, 170)
elixir_pos = (190, 195, 370, 225)

gold_pos_TabS7 = (115, 170, 320, 215)
elixir_pos_TabS7 = (115, 240, 320, 285)


def conect():
    client = scrcpy.Client(device=adb.device())
    client.start(threaded=True)
    client.max_fps = 3
    while client.last_frame is None:
        time.sleep(0.01)
    return client

def search(client):
    client.control.touch(res[0] - 0.15 * res[0], res[1] - 0.2 * res[1], scrcpy.ACTION_DOWN)
    client.control.touch(res[0] - 0.15 * res[0], res[1] - 0.2 * res[1], scrcpy.ACTION_UP)
    print((res[0] - 0.15 * res[0], res[1] - 0.2 * res[1]))


def scan_image(pos):
    result_img = img.crop(pos)

    result_img.save("/home/jo/Dokumente/Private/Projects/easyocrTest/result1.jpg")
    result_img = result_img.point(lambda x: 255 if x > 190 else 0).convert('L')  # .filter(ImageFilter.EDGE_ENHANCE_MORE)
    # gold3 = Image.open("/home/jo/Dokumente/Private/Projects/easyocrTest/gold2.jpg")
    result_img.save('/home/jo/Dokumente/Private/Projects/easyocrTest/result2.jpg')
    for _ in range(1):
        result_img = result_img.filter(ImageFilter.MinFilter(3))

    for _ in range(1):
        result_img = result_img.filter(ImageFilter.MaxFilter(3))

    # gold3 = gold3.point(lambda x: 255 if x > 90 else 0)

    result_img.save('/home/jo/Dokumente/Private/Projects/easyocrTest/result3.jpg')

    result_list = reader.readtext('result3.jpg', detail=0, allowlist="1234567890")
    result = result_list

    return result


#res = #(2280,1080)client.resolution

client = conect()
input1 = ""
reader = easyocr.Reader(['en'], gpu=False)
res = client.resolution
while input1 == '':
    search(client)
    time.sleep(3)
    print("res:" + str(client.resolution))
    # img = Image.open('test0.jpg')
    result_gold = []
    while len(result_gold) == 0:
        img = Image.fromarray(client.last_frame)
        img.save("/home/jo/Dokumente/Private/Projects/easyocrTest/test0.jpg")
        result_gold = scan_image(gold_pos)
        result_elixir = scan_image(elixir_pos)

    print("--------------")

    if len(result_gold[0]) >= 7 and result_gold[0][0] != '1' and result_gold[0][0] != '1':
        result_gold[0] = result_gold[0][:len(result_gold[0])-2]

    if len(result_elixir[0]) >= 7 and result_elixir[0][0] != '1' and result_elixir[0][0] != '1':
        result_elixir[0] = result_elixir[0][:len(result_elixir[0]) - 2]

    print("Gold: " + str(result_gold[0]))
    print("Elixir: " + str(result_elixir[0]))
    print("Average: " + str(int(result_gold[0]) + int(result_elixir[0])))


    if (int(result_gold[0]) + int(result_elixir[0])) > 1600000:
        playsound("sound1.mp3")
        print("next?")
        input1 = input()




#print(scrcpy.ControlSender.get_clipboard())
#client.control.touch(1000,200, scrcpy.ACTION_DOWN)
client.stop()